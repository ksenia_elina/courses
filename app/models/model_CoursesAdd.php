<?php
class Model_CoursesAdd extends Model
{
    public $dbh;
    public function get_data()
    {
        $sth = $this->dbh->prepare("SELECT * FROM `courseslist`;");
        $sth->execute();
        $res = $sth->get_result();
        $result = $res->fetch_all();
        //var_dump($result);
        return $result;
    }
    //получаем инфу по фильтрам
    public function get_inf_fo_filter(string $code = "", string $dd = "")
    {
        if($code <> "" &&  $dd <> "")
        {
            $stmt = $this->dbh->prepare("SELECT * FROM `courseslist` where `CharCode` = ? AND `Date` = ?");
            mysqli_stmt_bind_param($stmt, 'ss', $code, $dd);
        }elseif($code <> ""){
            $stmt = $this->dbh->prepare("SELECT * FROM `courseslist` where `CharCode` = ?");
            mysqli_stmt_bind_param($stmt, 's', $code);
        }elseif($dd <> "")
        {
            $stmt = $this->dbh->prepare("SELECT * FROM `courseslist` where `Date` = ?");
            mysqli_stmt_bind_param($stmt, 's', $dd);
        }
        else
        {
            $stmt = $this->dbh->prepare("SELECT * FROM `courseslist`;");
        }

        $stmt->execute();
        //var_dump($idnews);
        $res = $stmt->get_result();
        $result = $res->fetch_all();
        //var_dump($result);
        return $result;
        
    }
    //добавляем в таблицу
    public function add_data($data,$date)
    {
        //some realization $data->Valute[3]
        $stmtn = $this->dbh->prepare("SELECT * FROM `courseslist` where `Date` = ?");

        mysqli_stmt_bind_param($stmtn, 's', date("Y-m-d H:i:s", strtotime($date)));

        $status = $stmtn->execute();
        $stmtn->store_result();
        if ($stmtn->num_rows == 0) { 
        $stmtn->close();
            for($i = 0; $i< count($data->Valute); $i++)
            {
                $stmt = $this->dbh->prepare("INSERT INTO `courseslist` (`id`, `NumCode`, `CharCode`, `Nominal`, `Name`, `Value`, `Date`, `IDC`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?);");
                mysqli_stmt_bind_param($stmt, 'ssssdss', $data->Valute[$i]->NumCode, $data->Valute[$i]->CharCode,$data->Valute[$i]->Nominal, $data->Valute[$i]->Name, $this->tofloat($data->Valute[$i]->Value), date("Y-m-d H:i:s", strtotime($date)),$data->Valute[$i]['ID'] );
                

                $stmt->execute();
            }
        }
    }
    
    //из документации
    public function tofloat($num) {
    $dotPos = strrpos($num, '.');
    $commaPos = strrpos($num, ',');
    $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
        ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
  
    if (!$sep) {
        return floatval(preg_replace("/[^0-9]/", "", $num));
    }

    return floatval(
        preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
        preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
    );
}
}
?>
