<?php
class Controller_Dynamic extends Controller
{

	function __construct()
	{
		//$this->model = new Model_CoursesAdd();
		$this->view = new View();
	}
	//получение динамики
	function action_index()
	{
        //xml from site
        $req_date = date('d/m/y');
        date_default_timezone_set('Europe/Moscow');
                // 1. инициализация
        $ch = curl_init();
        $url = 'http://www.cbr.ru/scripts/XML_dynamic.asp';
       // http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=02/03/2001&date_req2=14/03/2001&VAL_NM_RQ=R01235

    $url = $url."?date_req1=".date("d/m/Y", strtotime("02/03/2001"))."&date_req2=".date('d/m/Y')."&VAL_NM_RQ=".$_GET['VAL_NM_RQ'];
        // 2. указываем параметры, включая url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // 3. получаем HTML в качестве результата
        $output = curl_exec($ch);

        // 4. закрываем соединение
        curl_close($ch);
        $obj = simplexml_load_string ($output); 
        //var_dump($obj->Record[0]["Date"][0]->__toString());
       // var_dump($obj->Record[0]->Value);
        //var_dump($obj->Valute[3]['ID']);
        //var_dump($this->tofloat($obj->Valute[3]->Value));
        $dym = array();
        for($i = 0; $i< count($obj->Record); $i++)
        {
            $dym[] = array($obj->Record[$i]["Date"]->__toString(), floatval ($obj->Record[$i]->Value->__toString()));
        }
       $data = json_encode($dym);
       // если есть save, то генерируем сохранение, иначе просто график
       If(isset($_GET['save']))
       {
            $fd = fopen("output.json", 'w') or die("не удалось создать файл");
            fwrite($fd, $data);
            fclose($fd);
            $this->view->generate('Dynamic_view.php', 'template_viewheaders.php',$data);
       }
       else
       {
            $this->view->generate('Dynamic_view.php', 'template_view.php',$data);
       }
	}
    


}
?>