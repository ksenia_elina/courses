<?php
class Controller_CoursesAdd extends Controller
{

	function __construct()
	{
		$this->model = new Model_CoursesAdd();
		$this->view = new View();
	}
	//загрузка автоматическая при заходе на сайт
	function action_index()
	{
        //xml from site
        $req_date = date('d/m/y');
        date_default_timezone_set('Europe/Moscow');
                // 1. инициализация
        $ch = curl_init();
        $url = 'http://www.cbr.ru/scripts/XML_daily.asp';
        if($req_date <> date('d/m/y'))
        {
            $url = $url."?date_req=".$req_date;
        }
        // 2. указываем параметры, включая url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // 3. получаем HTML в качестве результата
        $output = curl_exec($ch);

        // 4. закрываем соединение
        curl_close($ch);
        $obj = simplexml_load_string ($output); 
        //var_dump($obj->Valute[3]['ID']);
        //var_dump($this->tofloat($obj->Valute[3]->Value));
		$this->model->add_data($obj, date("Y-m-d"));
        $data = $this->model->get_data();		

		$this->view->generate('CoursesAdd_view.php', 'template_view.php', $data);
	}
    //фильтрация
    function action_filter()
    {
        if(isset($_GET['ch']) && isset($_GET['dd']))
        {
            $data = $this->model->get_inf_fo_filter($_GET['ch'], $_GET['dd']);
        }elseif(isset($_GET['ch']))
        {
            $data = $this->model->get_inf_fo_filter($_GET['ch']);
        }
        elseif(isset($_GET['dd']))
        {
            $data = $this->model->get_inf_fo_filter( "", $_GET['dd']);
        }
        else
        {
            $data = $this->model->get_inf_fo_filter();
        }
        $this->view->generate('CoursesAdd_view.php', 'template_view.php', $data);
    }
    //ручная загрузка
    function action_daycourse()
	{
        //xml from site
        $req_date = date("d/m/Y", strtotime($_POST['date']));
        date_default_timezone_set('Europe/Moscow');
                // 1. инициализация
        $ch = curl_init();
        $url = 'http://www.cbr.ru/scripts/XML_daily.asp';

        $url = $url."?date_req=".$req_date;
        // 2. указываем параметры, включая url
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // 3. получаем HTML в качестве результата
        $output = curl_exec($ch);

        // 4. закрываем соединение
        curl_close($ch);
        $obj = simplexml_load_string ($output); 
		$this->model->add_data($obj,$_POST['date']);	

		$this->view->generate('CoursesAddR_view.php', 'template_view.php');
	}


}
?>